
package com.mycompany.pptx.converter;

import com.aspose.slides.HtmlFormatter;
import com.aspose.slides.HtmlOptions;
import com.aspose.slides.ISlideCollection;
import com.aspose.slides.Presentation;
import com.aspose.slides.SaveFormat;
import java.io.File;

public class Main {
	public static void main(String[] args) {
		Presentation pres = new Presentation(args[0]);

                ISlideCollection c = pres.getSlides();
                
                HtmlOptions htmlOpt = new HtmlOptions();
                htmlOpt.setIncludeComments(true);
		htmlOpt.setHtmlFormatter(HtmlFormatter.createDocumentFormatter("", false));

                for (int i = 0; i < c.size(); ++i) {
                    int[] slides = new int[1];
                    slides[0] = i + 1;
                    
                    File dirSlides = new File("slides");
                    if (!dirSlides.exists())
                        dirSlides.mkdir();
                    
                    pres.save(
                            "slides\\slide" + i + ".html",
                            slides,
                            SaveFormat.Html, 
                            htmlOpt
                    );                                                           
                }
	}     
}