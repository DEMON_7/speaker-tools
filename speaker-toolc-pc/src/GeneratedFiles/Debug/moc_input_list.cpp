/****************************************************************************
** Meta object code from reading C++ file 'input_list.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gui/input_list.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'input_list.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_InputListModel_t {
    QByteArrayData data[14];
    char stringdata0[179];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_InputListModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_InputListModel_t qt_meta_stringdata_InputListModel = {
    {
QT_MOC_LITERAL(0, 0, 14), // "InputListModel"
QT_MOC_LITERAL(1, 15, 13), // "showInterface"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 9), // "okClicked"
QT_MOC_LITERAL(4, 40, 13), // "cancelClicked"
QT_MOC_LITERAL(5, 54, 6), // "hideOk"
QT_MOC_LITERAL(6, 61, 10), // "hideCancel"
QT_MOC_LITERAL(7, 72, 14), // "showAllButtons"
QT_MOC_LITERAL(8, 87, 12), // "requestInput"
QT_MOC_LITERAL(9, 100, 31), // "QList<QPair<QStringList,bool> >"
QT_MOC_LITERAL(10, 132, 8), // "clearAll"
QT_MOC_LITERAL(11, 141, 9), // "onClickOk"
QT_MOC_LITERAL(12, 151, 13), // "onClickCancel"
QT_MOC_LITERAL(13, 165, 13) // "onButtonClick"

    },
    "InputListModel\0showInterface\0\0okClicked\0"
    "cancelClicked\0hideOk\0hideCancel\0"
    "showAllButtons\0requestInput\0"
    "QList<QPair<QStringList,bool> >\0"
    "clearAll\0onClickOk\0onClickCancel\0"
    "onButtonClick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_InputListModel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,
       3,    0,   70,    2, 0x06 /* Public */,
       4,    0,   71,    2, 0x06 /* Public */,
       5,    0,   72,    2, 0x06 /* Public */,
       6,    0,   73,    2, 0x06 /* Public */,
       7,    0,   74,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   75,    2, 0x0a /* Public */,
      10,    0,   76,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      11,    0,   77,    2, 0x02 /* Public */,
      12,    0,   78,    2, 0x02 /* Public */,
      13,    1,   79,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    0x80000000 | 9,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void InputListModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        InputListModel *_t = static_cast<InputListModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->showInterface(); break;
        case 1: _t->okClicked(); break;
        case 2: _t->cancelClicked(); break;
        case 3: _t->hideOk(); break;
        case 4: _t->hideCancel(); break;
        case 5: _t->showAllButtons(); break;
        case 6: { QList<QPair<QStringList,bool> > _r = _t->requestInput();
            if (_a[0]) *reinterpret_cast< QList<QPair<QStringList,bool> >*>(_a[0]) = std::move(_r); }  break;
        case 7: _t->clearAll(); break;
        case 8: _t->onClickOk(); break;
        case 9: _t->onClickCancel(); break;
        case 10: _t->onButtonClick((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (InputListModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&InputListModel::showInterface)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (InputListModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&InputListModel::okClicked)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (InputListModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&InputListModel::cancelClicked)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (InputListModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&InputListModel::hideOk)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (InputListModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&InputListModel::hideCancel)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (InputListModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&InputListModel::showAllButtons)) {
                *result = 5;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject InputListModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_InputListModel.data,
      qt_meta_data_InputListModel,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *InputListModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *InputListModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_InputListModel.stringdata0))
        return static_cast<void*>(this);
    return QAbstractListModel::qt_metacast(_clname);
}

int InputListModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void InputListModel::showInterface()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void InputListModel::okClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void InputListModel::cancelClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void InputListModel::hideOk()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void InputListModel::hideCancel()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void InputListModel::showAllButtons()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
