
#pragma once

#include "../src/utils/global-signals.h"

#include <qtcpserver.h>
#include <qhostaddress.h>
#include <qtcpsocket.h>
#include <qlist.h>
#include <qmap.h>

#include "client_thread.h"

// singltone pattern used 
class Server : public QTcpServer {
	Q_OBJECT 

		// static interface
public: 
	static Server* m_instance;
	static Server* createInstance(int port, QObject* parent = nullptr);
	static void setInstance(Server*);	
	static Server* getInstance();

		// nonstatic interface
public:
	explicit Server(QObject *parent = nullptr);	
	ClientThread* getClientThread(int id);
	void debugLog(const QString& method, const QString &info);
	
	Q_SLOT void onNewConnection();
	Q_SLOT void onBCastNameChanged(const QString&);

protected:
	bool downloadStartPage();
	bool downloadFavicon();

private:
	QHostAddress m_addr;
	quint32 m_port;
	QMap<int, ClientThread*> m_threads;
	QByteArray m_html_start_page;
	QByteArray m_favicon;
	int m_id_counter = 1; // counter of user's ids
};