
#pragma once

#include <qthread.h>
#include <qtcpsocket.h>
#include <../src/utils/log.h>

class ClientThread : public QThread {
	Q_OBJECT

public:
	explicit ClientThread(QTcpSocket* socket, const QByteArray &html_start_page, const QByteArray &favicon, QObject *parent = nullptr);

	Q_SLOT void onReceived();
	Q_SLOT void processGet(const QString &query);

	Q_SLOT void send(const QByteArray&);
	Q_SLOT void onSlideChanged(const int slideIndex);
	
protected:
	void sendStartPage();
	void sendCurrentSlide();
	void sendFavicon();
	void redirectToStartPage();
	QString getQuery(const QString &html);

private:
	int m_actual_slide;
	const QByteArray m_html_start_page;
	const QByteArray m_favicon;
	QTcpSocket * m_socket;
};