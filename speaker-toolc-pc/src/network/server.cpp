
#include "server.h"

#include <qthread.h>
#include <qtimer.h>

#include "../src/utils/log.h"
#include "../src/gui/presentation.h"
#include "message-packer.h"
#include "parser-html.h"
#include "utils/settings.h"

Server* Server::m_instance = nullptr;

Server::Server(QObject * parent)
	: QTcpServer(parent)
{

}

Server* Server::createInstance(int port, QObject *parent) {			
	m_instance = new Server(parent);
	if (!m_instance->listen(QHostAddress::Any, port)) {		
		m_instance->debugLog("createInstance", " listening start is failed");
		return nullptr;
	}

	m_instance->debugLog("createInstance", " listening is started with " + QString::number(port));

	auto thread = new QThread(parent);
	thread->setObjectName("Server thread");
	m_instance->moveToThread(thread);
	connect(thread, &QThread::quit, m_instance, &QObject::deleteLater);			
	connect(m_instance, &QTcpServer::newConnection, m_instance, &Server::onNewConnection);	

	Q_ASSERT(m_instance->downloadStartPage());
	Q_ASSERT(m_instance->downloadFavicon());

	thread->start();

	connect(GlobalSignals::getInstance(), &GlobalSignals::broadcastNameChanged, m_instance, &onBCastNameChanged);
}

void Server::setInstance(Server* instance) {
	Q_ASSERT(instance);
	m_instance = instance;
}

Server* Server::getInstance() {
	Q_ASSERT(m_instance);
	return m_instance;
}

ClientThread * Server::getClientThread(int id) {
	if (m_threads.contains(id))
		return m_threads[id];
	else
		return nullptr;
}

void Server::debugLog(const QString & method, const QString & info) {
	Log::logDebug(method, info);
}

void Server::onNewConnection() {
	auto socket = nextPendingConnection();

	if (socket) {
		debugLog("onNewConnection", "new connection with local addres " + socket->localAddress().toString()
			+ " peer address " + socket->peerAddress().toString()
			+ " port " + socket->peerPort()
		);

		m_threads.insert(m_id_counter++, new ClientThread(socket, m_html_start_page, m_favicon, this));
	} 
	else {
		debugLog("onNewConnection", "slot emitted but hasn't available connections");
		return;
	}
}

void Server::onBCastNameChanged(const QString &name) {
	HtmlParser::insertBroadcastName(m_html_start_page, name);
}

bool Server::downloadStartPage() {
	QFile file_html(":/web/start-page.html");

	if (!file_html.open(QIODevice::ReadOnly))
		return false;

	m_html_start_page = file_html.readAll();

	return true;
}
bool Server::downloadFavicon() {
	QFile file_ico(":/icons/favicon.ico");

	if (!file_ico.open(QIODevice::ReadOnly))
		return false;

	m_favicon = file_ico.readAll();

	return true;
}

