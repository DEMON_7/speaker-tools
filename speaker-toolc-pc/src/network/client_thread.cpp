
#include "client_thread.h"
#include "message-packer.h"
#include <qregexp.h>
#include <qstring.h>
#include "../src/gui/presentation.h"
#include "../src/utils/global-signals.h"
#include <utils/session.h>
#include "parser-html.h"

#include <qhostaddress.h>
#include <qtimer.h>

ClientThread::ClientThread(QTcpSocket *socket, const QByteArray &html_start_page, const QByteArray &favicon, QObject *parent)
	: QThread(parent), m_socket(socket), m_html_start_page(html_start_page), m_favicon(favicon)
{
	if (!socket) {
		Log::logDebug("ClientThred-constructor", "ClientThread got nullptr instead socket");
		return;
	}

	setObjectName("Client's thread for peer address " + socket->peerAddress().toString());

	connect(socket, &QTcpSocket::readyRead, this, &ClientThread::onReceived);
	connect(socket, &QTcpSocket::disconnected, this, &QThread::quit);
	connect(this, &QThread::quit, this, &QThread::deleteLater);	

	sendStartPage();
		
	m_actual_slide = Presentation::currentSlide();

	start();

	connect(GlobalSignals::getInstance(), &GlobalSignals::slideChanged,
		this, &ClientThread::onSlideChanged);
}

void ClientThread::onReceived() {
	auto bytes = m_socket->readAll();
	QString html(bytes);

	Log::logDebug("ClientThread::onReceived", "Message received: " + bytes);

	auto query = getQuery(html);

	if (query == "authorization") {
		int pos = html.indexOf("=");
		QByteArray name_bytes; 
		for (int i = pos + 1; html[i] != ' '; ++i)
			if (html[i] != '%')
				name_bytes.push_back(bytes[i]);

		auto name = name_bytes;

		Log::logForUser("onReceived", "client was connected with name " + name);	

		Session::addUser(name);

		redirectToStartPage();
	}
	else if (query == "favicon") {
		sendFavicon();
	}
	else if (query == "authorized") {
		sendCurrentSlide();
	}
}

void ClientThread::processGet(const QString & query) {
	
}

void ClientThread::send(const QByteArray& message) {
	if (!m_socket || !m_socket->isOpen())
		return;
	else {
		m_socket->write(message);
		return;
	}
}

void ClientThread::onSlideChanged(const int slideIndex) {
	sendCurrentSlide();
}

void ClientThread::sendStartPage() {
	send(MessagePacker::pack(
		200,
		m_html_start_page,
		"start-page.html"
	));
}

void ClientThread::sendCurrentSlide() {
	send(MessagePacker::pack(
		200,
		HtmlParser::insertMetaRefresh(Presentation::getSlide(Presentation::currentSlide()), 5000),
		"slide0.html")
	);
}

void ClientThread::sendFavicon() {
	send(MessagePacker::pack(
		200,
		m_favicon,
		"favicon.ico"
	));
}

void ClientThread::redirectToStartPage() {
	send(MessagePacker::pack(
		303,
		"",
		"authorized.exe"
	));
}

QString ClientThread::getQuery(const QString & html) {
	int start = html.indexOf("/");
	int end = html.indexOf(".");
	if (start == -1 || end == -1)
		return QString();

	QString query;
	for (int i = start + 1; i < end; ++i)
		query.push_back(html[i]);

	return query;
}
