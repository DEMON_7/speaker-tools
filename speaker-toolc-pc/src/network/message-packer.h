
#pragma once

#include <qstring.h>
#include <qdatetime.h>

class MessagePacker {
public:

	static QByteArray pack(int hhtp_code, const QByteArray &body, const QString &location = "") {
		QByteArray m;

		switch (hhtp_code) {
			case 200: 
				m += "HTTP/1.1 200 OK\r\n";
				m += "Date: " + QDateTime().currentDateTime().toString() + "\r\n";
				m += "Connection: keep-alive\r\n";
				m += "Content-Type: text/html; charset=utf-8\r\n";
			    m += "Content-Length: " + QString::number(body.size()) + "\r\n\r\n";
				m.append(body);			
			break;

			case 303: 
				m += "HTTP/1.1 303 See Other\r\n";
				m += "Date: " + QDateTime().currentDateTime().toString() + "\r\n";
				m += "Connection: keep-alive\r\n";
				m += "Content-Type: text/html; charset=utf-8\r\n";
				m += "Location: " + location + "\r\n";
				m += "Content-Length: " + QString::number(body.size()) + "\r\n\r\n";
				m.append(body);
			break;
		}

		return m;
	}
};