
#pragma once

#include <qbytearray.h>
#include <qstring.h>
#include <qfile.h>

// singltone pattern
class HtmlParser {
public:

	// ������� ������� ������������� ������ � ��������
	static QByteArray insertMetaRefresh(QByteArray &data, int timeout) {
		int pos_to_insert = data.indexOf("<head>") + 6;
		
	    QFile ajax_creation(":/web/javax-query-creation.js");
	    QFile ajax_update(":/web/update-slide.js");

		Q_ASSERT(ajax_creation.open(QIODevice::ReadOnly));
		Q_ASSERT(ajax_update.open(QIODevice::ReadOnly));

		auto query_creator = ajax_creation.readAll();
		auto query_update = ajax_update.readAll();

		data.insert(pos_to_insert,
			"<script>"

			+ query_creator 
			+ query_update  +

			"setTimeout(function() { " 
			"update();" 
	        "}, " + QString::number(timeout) + ");"
			"</script>"
		);		
		
		return data;
	}	

	// �������� ������ ���������� �� �������-��������� 
	static QByteArray removeCopyright(QByteArray &data) {
		data.replace("Evaluation only.", "");
		data.replace("Created with Aspose.Slides for Java 18.8.", "");
		data.replace("Copyright 2004-2018 Aspose Pty Ltd.", "");

		return data;
	}	

	static void insertBroadcastName(QByteArray &html, const QString &name) {
		const QString mark_to_insert = "BroadcastName";
		int pos = html.indexOf(mark_to_insert);
		html.remove(pos, mark_to_insert.size());
		html.insert(pos, name);
	}
};