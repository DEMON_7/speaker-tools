
#pragma once

class QString;
class QMutex;

class Log {
public:
  Log() = delete;
  
  static void logDebug(const QString & source, const QString & message); // ���������� ��������� � ������ �������    
  static void logRelease(const QString & source, const QString & message); // ���������� ��������� � ������ ������
  static void logForUser(const QString & source, const QString & message); // ���������� ��������� � ����������� ������������

  static QMutex *m_mutex;
};
