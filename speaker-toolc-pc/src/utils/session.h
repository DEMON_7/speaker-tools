
#pragma once

#include <qstring.h>
#include <qstringlist.h>
#include <qmutex.h>

class Session {

public:
	static void addUser(const QString &user_name);
	static QStringList getUsers();
	static QStringList getIPV4Address();

public:
	static QStringList m_users; 
	static QMutex m_mutex;
};