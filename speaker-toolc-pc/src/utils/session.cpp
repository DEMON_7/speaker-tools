
#include "session.h"

#include <qnetworkinterface.h>

QStringList Session::m_users = {};
QMutex Session::m_mutex;

void Session::addUser(const QString & user_name) {
	QMutexLocker locker(&m_mutex);
	m_users << user_name;
}

QStringList Session::getUsers() {
	return m_users;
}

QStringList Session::getIPV4Address() {
	QStringList ips;
	for (const auto &i : QNetworkInterface::allAddresses())
		if (i.protocol() == QAbstractSocket::IPv4Protocol && i != QHostAddress(QHostAddress::LocalHost))
			ips << i.toString();

	return ips;
}
