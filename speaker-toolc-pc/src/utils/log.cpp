
#include "log.h"

#include <QString>
#include <QDebug>
#include <qmutex.h>

QMutex* Log::m_mutex = nullptr;

void Log::logDebug(const QString & source, const QString & message) {
#ifdef QT_DEBUG
	QMutexLocker locker(m_mutex);
	qDebug() << source << "  " << message;
#endif
}

void Log::logForUser(const QString & source, const QString & message) {
	logDebug(source, message);
}

void Log::logRelease(const QString & source, const QString & message) {

}
