
#pragma once

#include <qstring.h>
#include <qmap.h>
#include <qvariant.h>

class Settings {
	
	// static interface
public:
	static void set(const QString &id, QVariant value);
	static QVariant get(const QString &id);
	
	// data 
public:
	static QMap< QString, QVariant> m_data;
};