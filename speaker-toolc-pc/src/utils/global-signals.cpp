
#include "global-signals.h"

GlobalSignals* GlobalSignals::instance_ = nullptr;

GlobalSignals::GlobalSignals(QObject *parent)
	: QObject(parent) {

}

void GlobalSignals::setInstance(GlobalSignals* p) {
	Q_ASSERT(p);
	instance_ = p;
}

GlobalSignals* GlobalSignals::getInstance() {
	Q_ASSERT(instance_ != nullptr);

	return instance_;
}