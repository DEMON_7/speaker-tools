
#pragma once

#include <QFileDialog>
#include <QObject>

#include "../gui/input_list.h"
#include "../gui/presentation.h"
#include "../utils/log.h"

class StartDialog {
public:
  StartDialog() = delete;

  static void exec(Presentation *presentation_view);
};
