
#include "gui-core.h"

#include <QQuickWidget>
#include <QString>
#include <QUrl>
#include <QMessageBox>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QObjectList>
#include <QFileDialog>

#include "../utils/constants.h"
#include "../utils/log.h"
#include "../gui/start-dialog.h"
#include "../utils/global-signals.h"
#include "presentation.h"

GuiCore::GuiCore(const QString& view_name, QObject *parent)
  : QObject(parent)
{
  qmlRegisterType<InputListModel>("CustomModels", 1, 0, "InputListModel");

  qml_engine_ = new QQmlApplicationEngine(view_name);

  qml_engine_->rootContext()->setContextProperty("gui_core", this);
  qml_engine_->rootContext()->setContextProperty("constants", new Constants());

  connect(GlobalSignals::getInstance(), &GlobalSignals::startDialogEnded,
	  this, &GuiCore::hide);

  connect(GlobalSignals::getInstance(), &GlobalSignals::runtimeDialogStarted,
	  this, &GuiCore::show);

  connect(GlobalSignals::getInstance(), &GlobalSignals::runtimeDialogEnded,
	  this, &GuiCore::hide);

}


bool GuiCore::init() {
  initModels();
  initConnections();

//  auto files_list = qobject_cast<FilesListModel*>( models_["files_list"] );

  return true;
}

void GuiCore::start() {
  init();

  StartDialog::exec( (Presentation*) models_["presentation_model"]);
}

void GuiCore::onClosing() {

}

void GuiCore::initModels() {
  auto basic_object = qml_engine_->rootObjects().first();

  models_["input_list"] = basic_object->findChild<QObject*>("InputListModel");
  models_["presentation_model"] = new Presentation();

  InputList::init(qobject_cast<InputListModel*>(models_["input_list"]));

  qml_engine_->rootContext()->setContextProperty("presentation_model", models_["presentation_model"]);

#ifdef QT_DEBUG
  for (auto i : models_.values())
    Q_ASSERT(i != nullptr);
#endif
}

void GuiCore::initConnections() {

}

void GuiCore::debugInfo(const QString& method, const QVariant& arg) {
	Log::logDebug(method, arg.toString());
}

