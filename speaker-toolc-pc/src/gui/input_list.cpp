
#include "input_list.h"
#include "../utils/log.h"

#include <QDebug>
#include <QEventLoop>

InputListModel* InputList::model_ = nullptr;

void InputList::init(InputListModel *model) {
  Q_ASSERT(model);
  model_ = model;
}

QList<QPair<QStringList, bool> > InputList::requestInput() {
  return model_->requestInput();
}

void InputList::addItem(const QString &tooltip, InputListModel::ItemTypes type, const QString tooltip_2) {
  Q_ASSERT(model_);
  model_->addItem(tooltip, type, nullptr, tooltip_2);
}

InputListModel::InputListModel(QObject *parent) 
	: QAbstractListModel(parent) {
  setObjectName("InputListModel");
}

QList<QPair<QStringList, bool>> InputListModel::requestInput() {
  emit showInterface();

  QEventLoop loop;
  connect(this, &InputListModel::okClicked, &loop, &QEventLoop::quit);
  connect(this, &InputListModel::cancelClicked, &loop, &QEventLoop::quit);
  loop.exec();

  QList<QPair<QStringList, bool>> user_data;
  for (const auto &i : data_)
    user_data.push_back(QPair<QStringList, bool>(QStringList() << i.tooltip_ << i.in_text_, i.flag_));

  clearAll();
  return user_data;
}

void InputListModel::addItem(const QString &tooltip, InputListModel::ItemTypes type, void(*handler)(), const QString tooltip_2) {
  debugInfo("AddItem", tooltip + " " + QString::number(type));

  beginInsertRows(QModelIndex(), data_.count(), data_.count());
  data_.push_back(ListElement(tooltip, tooltip_2, "", false, type, handler));
  endInsertRows();
}

void InputListModel::onClickOk() {
  debugInfo("onClickOk", "");
  emit okClicked();
}

void InputListModel::onClickCancel() {
  debugInfo("onClickCancel", "");
  clearAll();
  emit cancelClicked();
}

void InputListModel::onButtonClick(int index) {
  const ListElement item = data_.at(index);
  item.clickHandler();
}

int InputListModel::rowCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  return data_.count();
}

QVariant InputListModel::data(const QModelIndex &index, int role) const {
    debugInfo("data", QString::number(index.row()) + " " + QString::number(role));

    const ListElement item = data_.at(index.row());

    switch (role) {
      case DoneRole: return QVariant(item.flag_);
      case ToolTipTole: return QVariant(item.tooltip_);
	  case SecondTooltipRole: return QVariant(item.tooltip_2_);
      case InTextRole: return QVariant(item.in_text_);
      case TypeRole: return QVariant(item.type_);
      case IndexRole: return QVariant(index.row());
    }

    return QVariant();
}

bool InputListModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    debugInfo("setData", value.toString());

    switch (role) {
     case DoneRole: data_[index.row()].flag_ = value.toBool(); break;
     case InTextRole: data_[index.row()].in_text_ = value.toString(); break;
    }

    emit dataChanged(index, index, QVector<int>() << role);
    return true;
}

Qt::ItemFlags InputListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> InputListModel::roleNames() const {
    QHash<int, QByteArray> names;
    names[DoneRole] = "done";
    names[ToolTipTole] = "tooltip";
	names[SecondTooltipRole] = "tooltip_2";
    names[InTextRole] = "intext";
    names[TypeRole] = "type";
    names[IndexRole] = "index";
    return names;
}

void InputListModel::clearAll() {
  if (data_.count() == 0) return;

  debugInfo("ClearAll", " cleaning...");
  beginRemoveRows(QModelIndex(), 0, data_.count() - 1);
  data_.clear();
  endRemoveRows();
}

void InputListModel::hideButton(Buttons b) {
	switch (b) {	
	case Buttons::Ok: emit hideOk(); return;
	case Buttons::Cancel: emit hideCancel(); return;
	}
}

void InputListModel::updateAt(int index, const QString & tooltip, const QString tooltip_2, const QString input) {
	if (index >= data_.size())
		return;

	auto item = data_.at(index);

	beginResetModel();

	item.tooltip_ = tooltip;
	
	if (tooltip_2 != QString())
		item.tooltip_2_ = tooltip_2;

	if (input != QString())
		item.in_text_ = input;

	endResetModel();
}

void InputListModel::removeAt(int index) {
	if (index >= data_.size())
		return;

	beginRemoveRows(QModelIndex(), data_.size() - 1, data_.size() - 1);
	data_.pop_back();
	endRemoveRows();
}

void InputListModel::setText(const QString &text, const int index) {
  Q_ASSERT(index >= 0 || index < data_.size());
  data_[index].tooltip_ = text;

  dataChanged(createIndex(index, 0), createIndex(index, 0));
}

void InputListModel::debugInfo(const QString &method, const QVariant &arg) const {
  Log::logDebug(method, arg.toString());
}

void InputList::addButton(const QString &tooltip, void(*handler)()) {
	Q_ASSERT(model_);
  
  model_->addItem(tooltip, InputListModel::ItemTypes::Button, handler);
}

void InputList::hideButton(InputListModel::Buttons b) {
	model_->hideButton(b);
}

void InputList::updateAt(int index, const QString & tooltip, const QString tooltip_2, const QString input) {
	model_->updateAt(index, tooltip, tooltip_2, input);
}

void InputList::removeAt(int index) {
	model_->removeAt(index);
}

void InputList::clearAll() {
	model_->clearAll();
	model_->showAllButtons();
}

void InputList::setText(const QString &text, const int index) {

}
