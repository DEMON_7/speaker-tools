
#pragma once

#include <qobject.h>
#include <qnetworkinterface.h>
#include <qstringlist.h>

#include "../src/gui/input_list.h"
#include "../src/utils/global-signals.h"
#include <utils/settings.h>
#include <utils/session.h>

class RuntimeDialog : public QObject {
	Q_OBJECT

public:
	RuntimeDialog() = delete;

	static void exec() {
		InputList::clearAll();
		InputList::addItem("Название трансляции", InputListModel::DoubleLine, Settings::get("broadcast-name").toString());			
		
		auto ips = Session::getIPV4Address();
		if (ips.size() == 0) 
			InputList::addItem("IPv4 адрес", InputListModel::DoubleLine, "устройство не подключенно к сети");

		for (const auto &i : ips)
			InputList::addItem("IPv4 адрес", InputListModel::DoubleLine, i);

		InputList::addItem("Участники", InputListModel::Line);

		for (const auto &i : Session::getUsers())
			InputList::addItem(i, InputListModel::Line);

		InputList::hideButton(InputListModel::Buttons::Cancel);
		GlobalSignals::getInstance()->runtimeDialogStarted();
		InputList::requestInput();	
		GlobalSignals::getInstance()->runtimeDialogEnded();
	}
};