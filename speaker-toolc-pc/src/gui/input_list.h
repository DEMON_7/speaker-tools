
#pragma once

#include <QObject>
#include <QAbstractListModel>
#include <QList>
#include <QPair>

class InputListModel;

class InputListModel : public QAbstractListModel {
  Q_OBJECT    

public:	
  explicit InputListModel(QObject *parent = nullptr);  

  enum Buttons {	 
	  Ok,
	  Cancel
  };

  enum {
     DoneRole = Qt::UserRole + 1,
     ToolTipTole,
	 SecondTooltipRole,
     InTextRole,
     TypeRole,
     IndexRole
  };

  enum ItemTypes {
	Line = 0, // 0 
	DoubleLine = Line + 1, // 1
    InputLine = DoubleLine + 1, // 2
    InputLineChecked = InputLine + 1, // 3
    CheckedLine = InputLineChecked + 1, // 4
    Button = CheckedLine + 1 // 5
  };

  // ������������ ������ ������(��������� � ��, ��� ���� ������������) � �������� ��������(���� ����)
  Q_SLOT QList<QPair<QStringList, bool>> requestInput();

  Q_SLOT void addItem(const QString &tooltip, ItemTypes type, void(*handler)() = nullptr, const QString tooltip_2 = "");
  Q_SLOT void clearAll();
 
  void hideButton(Buttons);
  void updateAt(int index, const QString& tooltip, const QString tooltip_2 = QString(), const QString input = QString());
  void removeAt(int index);
  void setText(const QString& text, const int index);

  Q_INVOKABLE void onClickOk();
  Q_INVOKABLE void onClickCancel();
  Q_INVOKABLE void onButtonClick(int);

  Q_SIGNAL void showInterface();
  Q_SIGNAL void okClicked();
  Q_SIGNAL void cancelClicked();
  Q_SIGNAL void hideOk();
  Q_SIGNAL void hideCancel();
  Q_SIGNAL void showAllButtons();

public:
     int rowCount(const QModelIndex &parent = QModelIndex()) const override;
     QVariant data(const QModelIndex &index, int role) const override;
     bool setData(const QModelIndex &index, const QVariant&, int role = Qt::EditRole) override;
     Qt::ItemFlags flags(const QModelIndex& index) const override;
     virtual QHash<int, QByteArray> roleNames() const override;

     void debugInfo(const QString&, const QVariant&) const;

private:
     struct ListElement {
       ListElement(const QString &tooltip,
				   const QString &tooltip_2,
                   const QString &default_value,
                   const bool default_check,
                   const int type,
                   void(*handler)() = nullptr
           ) :
           tooltip_(tooltip),
		   tooltip_2_(tooltip_2),
           in_text_(default_value),
           flag_(default_check),
           type_(type),
           clickHandler(handler) {}


       QString tooltip_;
	   QString tooltip_2_;
       QString in_text_;
       bool flag_;
       int type_;

       void (*clickHandler) ();
     };

     QList<ListElement> data_;
};

// ��������� �� ����������� ��������
class InputList {
public:
  // ������������� ������, ���������� �� InputList.qml 
  static void init(InputListModel* model);
  static QList<QPair<QStringList, bool>> requestInput();

  static void addItem(const QString &tooltip, InputListModel::ItemTypes type, const QString tooltip_2 = "");
  static void addButton(const QString &tooltip, void(*handler)());
  static void hideButton(InputListModel::Buttons);

  static void updateAt(int index, const QString& tooltip, const QString tooltip_2 = QString(), const QString input = QString());
  static void removeAt(int index);
  static void clearAll(); 
  static void setText(const QString& text, const int index);

private:
  static InputListModel* model_;
};