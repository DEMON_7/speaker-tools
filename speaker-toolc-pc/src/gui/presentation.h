
#pragma once

#include <qobject.h>
#include <qstring.h>
#include <qwebengineview.h>

class Presentation : public QObject {
  Q_OBJECT
public:
  explicit Presentation(QObject *parent = nullptr);  

public:   
   Q_SLOT bool nextSlide();
   Q_SLOT bool prevSlide();

   Q_SIGNAL void presentationSetted();
   Q_SIGNAL void slideChanged(int);

private:
  QWebEngineView* web_view_;
  QWidget * widget_;

  QWidget* createView();
  void showSlide(int);

  void clear();
  QString getFileName();

   // static 
public:
	static bool openPresentation(const QString &file_name);
	static QByteArray getSlide(int index);
	static int slidesCount();
	static int currentSlide();

private:
  static void downloadSlides(const QString &path);

   static int current_slide_;
   static QList<QByteArray> slides_; // html ������� ������ � �������� ���� 
   static QString current_file_name_;
   static QString slides_folder_name_;
   static Presentation* static_instance_;

public: 
  static QSize kToolbarButtonSize;
};