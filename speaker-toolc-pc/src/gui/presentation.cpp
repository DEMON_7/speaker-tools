
#include "presentation.h"
#include "../utils/global-signals.h"
#include "../utils/log.h"
#include "runtime-dialog.h"
#include <network/parser-html.h>

#include <qapplication.h>
#include <qprocess.h>
#include <qfile.h>
#include <qlayout.h>
#include <qtoolbutton.h>

QList<QByteArray> Presentation::slides_ = {};
QString Presentation::current_file_name_ = {};
QString Presentation::slides_folder_name_ = "slides";
Presentation* Presentation::static_instance_ = nullptr;
QSize Presentation::kToolbarButtonSize = QSize(40, 40);
int Presentation::current_slide_ = 0;

Presentation::Presentation(QObject *parent)
  : QObject(parent)
{
  Presentation::static_instance_ = this;

  auto signals_instance = GlobalSignals::getInstance();

  connect(this, &Presentation::presentationSetted, 
	  signals_instance, &GlobalSignals::presentationSetted);

  connect(this, &Presentation::slideChanged,
	  GlobalSignals::getInstance(), &GlobalSignals::slideChanged);

  widget_ = createView();
}

bool Presentation::openPresentation(const QString & file_name) {
  if (file_name == "") return false;

  current_file_name_ = file_name;

  auto app_path = QApplication::applicationDirPath();

  auto process_java = new QProcess(nullptr);
  process_java->setProgram("java");
  process_java->setArguments({ "-jar", QApplication::applicationDirPath() + "/" + slides_folder_name_ + "/converter.jar", file_name });
  process_java->start();
  process_java->waitForFinished();

  Log::logDebug("openPresentation", "Java converter is finished");

  QFile file(QApplication::applicationDirPath() + "/" + slides_folder_name_ + "/slide0.html");
  downloadSlides(QApplication::applicationDirPath() + "/" + slides_folder_name_);

  if (!slides_.isEmpty()) {
    static_instance_->showSlide(0);
    static_instance_->widget_->showFullScreen();
  }

  emit static_instance_->presentationSetted();
  return file.exists();
}

QByteArray Presentation::getSlide(int index) {
	return slides_.at(index);
}

 int Presentation::slidesCount() {
	return slides_.size();
}

  int Presentation::currentSlide() {
	 return current_slide_;
 }

bool Presentation::nextSlide() {
  if (Presentation::current_slide_ >= Presentation::slides_.size())
    return false;

  ++Presentation::current_slide_;
  Presentation::showSlide(Presentation::current_slide_);
  return true;
}

bool Presentation::prevSlide() {
  if (Presentation::current_slide_ < 0)
    return false;

  --Presentation::current_slide_;
  Presentation::showSlide(Presentation::current_slide_);
  return true;
}

QWidget * Presentation::createView() {
  widget_ = new QWidget();
  
  web_view_ = new QWebEngineView();
  web_view_->setZoomFactor(0.9);
  auto layout = new QVBoxLayout(widget_);  

  // right toolbar
  auto right_toolbar = new QWidget();
  right_toolbar->setMaximumWidth(100);
  auto layout_right_tlb = new QVBoxLayout(right_toolbar);
  auto button_settings = new QToolButton();
  button_settings->setIcon(QIcon(":/icons/settings.png"));
  button_settings->setIconSize({ 40, 40 });
  button_settings->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
  button_settings->setText(QString::fromLocal8Bit("Настройки"));
  layout_right_tlb->addWidget(button_settings);

  connect(button_settings, &QToolButton::clicked, [](){ 
	  RuntimeDialog::exec();
  });

  // lower toolbar
  auto toolbar = new QWidget();
  toolbar->setMaximumHeight(Presentation::kToolbarButtonSize.height());
  auto toolbar_layout = new QHBoxLayout(toolbar);
  auto button_forward = new QToolButton();
  auto button_back = new QToolButton();
  toolbar_layout->addWidget(button_back);
  toolbar_layout->addWidget(button_forward);
  button_back->setIcon(QIcon(":/icons/arrow_back.png"));
  button_forward->setIcon(QIcon(":/icons/arrow_forward.png"));
  button_back->resize(kToolbarButtonSize);
  button_forward->resize(kToolbarButtonSize);

  connect(button_forward, &QToolButton::clicked, this, &Presentation::nextSlide);
  connect(button_back, &QToolButton::clicked, this, &Presentation::prevSlide);

  // html-rightToolbar layout
  auto center_layout = new QHBoxLayout();
  center_layout->addWidget(web_view_);
  center_layout->addWidget(right_toolbar);

  layout->addLayout(center_layout);
  layout->addWidget(toolbar);

  return widget_;
}

void Presentation::showSlide(int slideIndex) {  
  if (slideIndex < 0 || slideIndex >= Presentation::slides_.size())
    return;
  
  emit slideChanged(slideIndex);

  Presentation::current_slide_ = slideIndex;
  static_instance_->web_view_->setHtml(Presentation::getSlide(slideIndex));
}


void Presentation::clear() {
  slides_.clear();
  current_file_name_ = "";
}

QString Presentation::getFileName() {
  return current_file_name_;
}

void Presentation::downloadSlides(const QString & path) {
  QFile file(path + "/slide0.html");
  int index = 1;
  while (file.open(QIODevice::ReadOnly)) {
    slides_.push_back(HtmlParser::removeCopyright(file.readAll()));
    file.close();
    file.setFileName(path + "/slide" + QString::number(index) + ".html");
    ++index;
  }
}
