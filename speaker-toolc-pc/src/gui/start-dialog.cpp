
#include "start-dialog.h"

#include <utils/settings.h>
#include <utils/session.h>
#include "../utils/global-signals.h"

void configureInputList() {
	InputList::clearAll();
	InputList::addItem("Название трансляции", InputListModel::InputLine);	
	
	auto ips = Session::getIPV4Address();

	if (ips.isEmpty()) {
		InputList::addItem("Не удалось определить локальный IP адрес", InputListModel::Line);
	}
	else {
		InputList::addItem("Ваш IP адрес", InputListModel::DoubleLine, ips.at(0));		
		for (int i = 1; i < ips.size(); ++i)
			InputList::addItem(ips.at(i), InputListModel::Line);	
	}

	InputList::hideButton(InputListModel::Buttons::Cancel);
}

void StartDialog::exec(Presentation * presentation_view) {		
	bool config_complete = false;
	while (!config_complete) {
		configureInputList();
		auto res = InputList::requestInput();

		Settings::set("broadcast-name", res[0].first[1]);	

		auto file_name = QFileDialog::getOpenFileName(
			nullptr,
			"Загрузка презентации",
			QDir::currentPath(),
			"*.pptx *.ppt"
		);
		
	    config_complete = Presentation::openPresentation(file_name);

		if (!config_complete)
			Log::logForUser("startDialog", "Presentation downloading is failed");
	}	

	GlobalSignals::getInstance()->startDialogEnded();
	GlobalSignals::getInstance()->broadcastNameChanged(Settings::get("broadcast-name").toString());
}
