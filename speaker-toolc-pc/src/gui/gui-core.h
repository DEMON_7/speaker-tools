
#pragma once

#include <QObject>
#include <QMap>

class QQmlApplicationEngine;

class GuiCore : public QObject {
  Q_OBJECT
public:
  explicit GuiCore(const QString& view_name, QObject* parent = nullptr);

  void start();

  Q_SIGNAL void hide();
  Q_SIGNAL void show();

protected:
  Q_INVOKABLE bool init();
  Q_INVOKABLE void onClosing();

  void initModels();
  void initConnections();

  inline void debugInfo(const QString &method, const QVariant& arg);

private:
  QQmlApplicationEngine* qml_engine_;
  QMap<QString, QObject*> models_; // input_list presentation_model
};
