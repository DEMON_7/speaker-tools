
#include <QApplication>
#include <QLabel>
#include <QQmlEngine>
#include <QQuickStyle>
#include <QVBoxLayout>
#include <qmainwindow.h>

#include <QFile>
#include <QTextStream>

#include "gui/gui-core.h"
#include "utils/global-signals.h"
#include <network/server.h>

int main(int argc, char *argv[]) {

  QApplication a(argc, argv);
  QQuickStyle::setStyle("Material");

  GlobalSignals::setInstance(new GlobalSignals());
  Server::createInstance(80);

  auto gui_core = new GuiCore("qrc:///GuiCore.qml", nullptr);
  gui_core->start();

  return a.exec();
}
