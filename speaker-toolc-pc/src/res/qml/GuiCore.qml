
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

ApplicationWindow {    

    minimumHeight: 480
    minimumWidth: 700
    maximumHeight: screen.height
    maximumWidth: screen.width

    visible: true
    title: constants.appName()
    onClosing: gui_core.onClosing()  		

    InputList{ }

	 Connections {
        target: gui_core

        onHide: {
			active: false
			visible: false
			close()
			console.log("Main window is hidden")
		}

		onShow: {
			show()
			active: true
			visible: true
			console.log("Main window is show")
		}
    }
}
