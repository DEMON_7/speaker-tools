import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

import CustomModels 1.0;

Item {
    id: input_list
    anchors.fill: parent

    property bool list_shown: false
	property bool cancel_enabled: true
	property bool ok_enabled: true
	property int tooltip_width: 200
	property int input_width: 200

    visible: list_shown

    InputListModel { id: input_list_model }

    Rectangle {
        id: list_background

        anchors.fill: parent
        opacity: 0.9
        color: "black"

        MouseArea {
            id: background_area
            anchors.fill: parent
            onClicked: {
                deactivate()
                input_list_model.onClickCancel()
            }
        }

        Frame {
            id: list_frame
            Material.theme: Material.Lime
            clip: true
            width: list_background.width * 0.7
            height: list_background.height * 0.7
            anchors.verticalCenter: list_background.verticalCenter
            anchors.horizontalCenter: list_background.horizontalCenter

            ListView {
                id: list
                model: input_list_model
                Material.theme: Material.Lime
                delegate: list_delegate
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: control_buttons.top
            }

            RowLayout {
                id: control_buttons
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                spacing: 0

                RoundButton {
                    id: button_cancel
                    flat: true
                    icon.source: "/icons/cancel.png"
                    Layout.alignment: Qt.AlignRight
				          	enabled: cancel_enabled

                    onClicked:  {						
                         deactivate()
                         input_list_model.onClickCancel()
                    }
                }

                RoundButton {
                    id: button_done
                    flat: true
                    icon.source: "/icons/done.png"
                    Layout.alignment: Qt.AlignRight
					enabled: ok_enabled

                    onClicked: {
                        deactivate()
                        input_list_model.onClickOk()
                    }
                }
            } // conrol buttons
        } // frame
    } // background   

    Component {
        id: list_delegate

        Row {
           // Layout.fillWidth: true
           width: list.width
           spacing: 10

           TextField {               // TOOLTIP
               id: element_tooltip
               text: model.tooltip
               activeFocusOnPress: false
               activeFocusOnTab: false
               hoverEnabled: false
               visible: model.type !== 5
			         width: tooltip_width
           }

		   TextField {			// TOOLTIP 2
			   id: tooltip_2
               text: model.tooltip_2
               activeFocusOnPress: false
               activeFocusOnTab: false
               hoverEnabled: false
               visible: model.type === 1
			   width: input_width
		   }

           TextField {              // INPUT TEXT
               id: element_input
               onEditingFinished: model.intext = text
               visible: model.type === 2 || model.type === 3
               Material.accent: Material.Blue
			   width: input_width

           }

           CheckBox {               // CHECKBOX
               id: element_check
               checked: model.done
               onClicked: model.done = checked
               visible: model.type === 3 || model.type === 4
               Material.accent: Material.Blue
           }

           Button {                 // BUTTON
                id: element_button
                Material.accent: Material.Blue
                text: model.tooltip
                visible: model.type === 5
                onClicked: input_list_model.onButtonClick(model.index)
           }
       }
    }

    function activate() {
        list_shown = true
    }

    function deactivate() {
        list_shown = false
    }

	 Connections {
        target: input_list_model

        onShowInterface : {
            activate()
        }

		onShowAllButtons: {
			console.log("All button are enabled")
			ok_enabled = true
			cancel_enabled = true
		}

		onHideOk: {
			console.log("Button ok is disabled")
			ok_enabled = true
		}

		onHideCancel: {
			console.log("Button cancel is disabled")
			cancel_enabled = false
		}
    }
}
