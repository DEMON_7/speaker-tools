
#include "log.h"

#include <QString>
#include <QDebug>
#include <qmutex.h>

QMutex* Log::m_mutex = nullptr;

void Log::logDebug(const QString &arg) {
#ifdef QT_DEBUG
	QMutexLocker locker(m_mutex);
	qDebug() << arg;
#endif
}

void Log::logForUser(const QString &m) {
	logDebug(m);
}
