
#pragma once

#include <QObject>

class QString;

class Constants : public QObject {
  Q_OBJECT

public:
  explicit Constants(QObject *parent = nullptr);

  Q_INVOKABLE QString appName() const; // имя приложения
};
