
#pragma once

#include <qobject.h>

// singletone class 
class GlobalSignals : public QObject {
	Q_OBJECT

	// static interface
public: 
	static GlobalSignals* instance_;
	static GlobalSignals* getInstance();
	static void setInstance(GlobalSignals* p);	

	// nonstatic interface
public:
	explicit GlobalSignals(QObject *parent = nullptr);

	Q_SIGNAL void presentationSetted(); // ����������� ���������� � ��������� 
	Q_SIGNAL void slideChanged(int);

	// dialogs
	Q_SIGNAL void startDialogEnded();
	Q_SIGNAL void runtimeDialogStarted();
	Q_SIGNAL void runtimeDialogEnded();
};
