
#include "settings.h"

QMap<QString, QVariant> Settings::m_data = {};

void Settings::set(const QString &id, QVariant value) {
	m_data[id] = value;
}

QVariant Settings::get(const QString &id) {
	if (m_data.contains(id))
		return m_data[id];
	else return { };
}

