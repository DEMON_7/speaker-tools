
#pragma once

class QString;
class QMutex;

class Log {
public:
  Log() = delete;
  
  static void logDebug(const QString&); // ���������� ��������� � ������ �������    
  static void logRelease(const QString&); // ���������� ��������� � ������ ������
  static void logForUser(const QString&); // ���������� ��������� � ����������� ������������

  static QMutex *m_mutex;
};
